package manual

var Name string = "manual"

func Parse(context []byte, args [][]byte) [][]byte {
	res := make([][]byte, 0)
	if len(context) < 4 {
		return res
	}
	sep := len(context) / 2
	res = append(res, context[0:sep])
	res = append(res, context[sep:])
	return res
}

func Check(context []byte, args [][]byte) bool {
	return false
}
